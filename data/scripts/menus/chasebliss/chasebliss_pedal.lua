-- Chase Bliss pedal
local chasebliss_pedal = {}

--local language_manager = require("scripts/language_manager")

function chasebliss_pedal:on_started()

  self.finished = true --false

  -- Load images.
  self.background_img = sol.surface.create("menus/chasebliss/pedal/background.png")
  self.background_img_w, self.background_img_h = self.background_img:get_size()

  self.pedal_img = sol.surface.create("menus/chasebliss/pedal/pedal.png")
  self.pedal_img_w, self.pedal_img_h = self.pedal_img:get_size()

  self.pedal_logo_sprite = sol.sprite.create("menus/chasebliss/pedal/pedal_logo")
  self.pedal_logo_sprite_w, self.pedal_logo_sprite_h = self.pedal_logo_sprite:get_size()

  self.shine_sprite = sol.sprite.create("menus/chasebliss/title_screen/small_shine")
  self.shine_sprite_w, self.shine_sprite_h = self.shine_sprite:get_size()
 
  -- Texts.
  --self.font, self.font_size = language_manager:get_dialog_font()
  self.text_color = { 255, 255, 255 }

  self.release_date_txt = sol.text_surface.create{
    color = self.text_color,
    horizontal_alignment = "center",
    font = "VCR_OSD_MONO",--self.font,
    font_size = 21,--self.font_size,
    text = "11.19.18",
  }

  -- Create clouds images.
  self.cloud_img_1 = sol.surface.create("menus/chasebliss/pedal/cloud_1.png")
  self.cloud_img_2 = sol.surface.create("menus/chasebliss/pedal/cloud_2.png")
  self.cloud_img_3 = sol.surface.create("menus/chasebliss/pedal/cloud_3.png")
  self.cloud_shapes = {
    self.cloud_img_1,
    self.cloud_img_2,
    self.cloud_img_3,
  }

  self.clouds_full_width = 480
  self.clouds_foreground = {
    { shape = 1,
      x = 2,
      y = 150,
    },
    -- { shape = 1,
    --   x = 160,
    --   y = 172,
    -- },
    { shape = 1,
      x = 120,
      y = 196,
    },
    { shape = 2,
      x = -200,
      y = 220,
    },
    -- { shape = 2,
    --   x = 220,
    --   y = 250,
    -- },
    { shape = 2,
      x = 520,
      y = 233,
    },
  }

  self.clouds_background = {
    { shape = 1,
      x = -4,
      y = 162,
    },
    { shape = 2,
      x = -112,
      y = 210,
    },
    { shape = 3,
      x = 162,
      y = 190,
    },
    { shape = 1,
      x = 240,
      y = 220,
    },
  }

  self.black_surface = sol.surface.create(480, 270)
  self.black_surface:fill_color({0, 0, 0})

  -- Make the clouds move.
  function move_foreground_clouds()
    -- Move all the clouds.
    for _, cloud in ipairs(self.clouds_foreground) do
      cloud.x = cloud.x + 1
      if cloud.x > self.clouds_full_width then
        cloud.x = -self.clouds_full_width / 2
      end
    end

    -- Repeat.
    sol.timer.start(self, 60, move_foreground_clouds)
  end
  -- Launch.
  move_foreground_clouds()

  function move_background_clouds()
    -- Move all the clouds.
    for _, cloud in ipairs(self.clouds_background) do
      cloud.x = cloud.x + 1
      if cloud.x > self.clouds_full_width then
        cloud.x = -self.clouds_full_width / 2
      end
    end

    -- Repeat.
    sol.timer.start(self, 120, move_background_clouds)
  end
  -- Launch.
  move_background_clouds()

  -- Start timer.
  self.anim_length = 2000
  self.anim_delta = 25
  self.elapsed_time = 0

  -- Start animation.
  self.step = 0
  self:step_1()

end

function chasebliss_pedal:step_1()
  self.step = 1
  self:reset_timer()

  self.black_surface:fade_out(20, function()
    self:step_2()
  end)
end

function chasebliss_pedal:step_2()
  self.step = 2
  self:reset_timer()
    
  self.elapsed_time = 0
  self.timer = sol.timer.start(self, self.anim_delta, function()
    -- Elapsed time since launch of animation.
    self.elapsed_time = self.elapsed_time + self.anim_delta

    if self.elapsed_time < self.anim_length then
      -- Keep on updating while time is remaining.
      return true
    else
      -- At the end of the animation, start the next step.
      self:step_3()
    end
  end)
end

function chasebliss_pedal:step_3()
  self.step = 3
  self:reset_timer()

  self.pedal_logo_sprite:set_animation("appearing", function()
    self:step_4()
  end)
end

function chasebliss_pedal:step_4()
  self.step = 4
  self:reset_timer()

  self.pedal_logo_sprite:set_animation("intermediate")

  self.timer = sol.timer.start(self, 500, function()  
    -- Catchphrase under the logo
    self.pedal_logo_sprite:set_animation("catchphrase", function()
      self:step_5()
    end)
  end)
end

function chasebliss_pedal:step_5()
  self.step = 5
  self:reset_timer()

  self.pedal_logo_sprite:set_animation("final")

  self.shine_sprite:set_animation("default", function()
  self.timer = sol.timer.start(500, function()
    self:step_6()
  end)
  end)
end

function chasebliss_pedal:step_6()
  self.step = 6
  self:reset_timer()

  self.release_date_txt:fade_in(60, function()
    self:step_7()
  end)
end

function chasebliss_pedal:step_7()
  self.step = 7
  self:reset_timer()

end

-- Resets the timer.
function chasebliss_pedal:reset_timer()
  self.elapsed_time = 0
  if self.timer ~= nil then
    self.timer:stop()
    self.timer = nil
  end
end

-- Easing function for movement.
function chasebliss_pedal:outQuart(t, b, c, d)
  t = t / d - 1
  return -c * (math.pow(t, 4) - 1) + b
end

function chasebliss_pedal:on_draw(dst_surface)

  local dst_w, dst_h = dst_surface:get_size()

  -- Background
  local background_img_x = (dst_w - self.background_img_w) / 2
  local background_img_y = (dst_h - self.background_img_h) / 2
  self.background_img:draw(dst_surface, background_img_x, background_img_y)

  -- Draw the clouds.
  for _, cloud in ipairs(self.clouds_background) do
    self.cloud_shapes[cloud.shape]:draw(dst_surface, cloud.x, cloud.y)
  end
  for _, cloud in ipairs(self.clouds_foreground) do
    self.cloud_shapes[cloud.shape]:draw(dst_surface, cloud.x, cloud.y)
  end

  -- Pedal
  if self.step >= 2 then
    local pedal_img_x = (dst_w - self.pedal_img_w) / 2 - 128
    local pedal_img_y = (dst_h - self.pedal_img_h) / 2
    if self.step == 2 then
      pedal_img_x = math.ceil(self:outQuart(self.elapsed_time, -self.pedal_img_w, pedal_img_x + self.pedal_img_w, self.anim_length))
    end 
    self.pedal_img:draw(dst_surface, pedal_img_x, pedal_img_y)    
    
      -- Shine
      if self.step >= 5 then
        self.shine_sprite:draw(dst_surface, pedal_img_x + self.pedal_img_w - 26, pedal_img_y + 4)
      end

      -- Logo
      if self.step >= 3 then
        local pedal_logo_sprite_x = (dst_w - self.pedal_logo_sprite_w) / 2 + 64
        local pedal_logo_sprite_y = (dst_h - self.pedal_logo_sprite_h) / 2
        self.pedal_logo_sprite:draw(dst_surface, pedal_logo_sprite_x, pedal_logo_sprite_y)
        
          -- Release date
          if self.step >= 6 then
            self.release_date_txt:draw(dst_surface, pedal_logo_sprite_x + self.pedal_logo_sprite_w / 2, pedal_logo_sprite_y + self.pedal_logo_sprite_h + 32)
          end
      end
  end

  -- Black surface.
  self.black_surface:draw(dst_surface, 0, 0)

end

-- Called when a keyboard key is pressed.
function chasebliss_pedal:on_key_pressed(key)

  if key == "escape" then
    -- Escape: quit Solarus.
    sol.main.exit()
    return true
  elseif self.finished then
    self:skip_menu()
    return true
  end
  
  return false
end

-- Mouse pressed: skip menu.
function chasebliss_pedal:on_mouse_pressed(button, x, y)
  if self.finished then
    if button == "left" or button == "right" then
      self:skip_menu()
      sol.main.exit()
    end
  end
end

-- Quits the menu.
function chasebliss_pedal:skip_menu()
  if not sol.menu.is_started(self) then
    return
  end

  -- Stop the timer.
  self:reset_timer()

  -- Quit the menu
  sol.menu.stop(self)
end

-- Return the menu to the caller
return chasebliss_pedal
