-- Chase Bliss title screen
local chasebliss_title = {}

function chasebliss_title:on_started()
  self.step = 0
  self.finished = true
  
  self.surface_w, self.surface_h = sol.video.get_quest_size()

  -- Load images.
  self.triforce_sprite = sol.sprite.create("menus/chasebliss/title_screen/triforce")
  self.triforce_sprite_w, self.triforce_sprite_h = self.triforce_sprite:get_size()

  self.logo_bliss_img = sol.surface.create("menus/chasebliss/title_screen/logo_bliss.png")
  self.logo_bliss_img_w, self.logo_bliss_img_h = self.logo_bliss_img:get_size()

  self.logo_tlo_img = sol.surface.create("menus/chasebliss/title_screen/logo_tlo.png")
  self.logo_tlo_img_w, self.logo_tlo_img_h = self.logo_tlo_img:get_size()

  self.logo_guitar_img = sol.surface.create("menus/chasebliss/title_screen/logo_guitar.png")
  self.logo_guitar_img_w, self.logo_guitar_img_h = self.logo_guitar_img:get_size()

  self.logo_subtitle_img = sol.surface.create("menus/chasebliss/title_screen/logo_subtitle.png")
  self.logo_subtitle_img_w, self.logo_subtitle_img_h = self.logo_subtitle_img:get_size()

  self.copyright_img = sol.surface.create("menus/chasebliss/title_screen/copyright.png")
  self.copyright_img_w, self.copyright_img_h = self.copyright_img:get_size()

  self.background_img = sol.surface.create("menus/chasebliss/title_screen/background.png")
  self.background_img_w, self.background_img_h = self.background_img:get_size()

  self.snes_img = sol.surface.create("menus/chasebliss/title_screen/snes.png")
  self.snes_img_w, self.snes_img_h = self.snes_img:get_size()
  
  self.shine_sprite = sol.sprite.create("menus/chasebliss/title_screen/small_shine")
  self.shine_sprite:set_xy(298, 140)
  
  self.press_start_sprite = sol.sprite.create("menus/chasebliss/title_screen/press_start")
  self.press_start_sprite_w, self.press_start_sprite_h = self.press_start_sprite:get_size()

  -- Black surface for the final fade-out
  self.black_surface = sol.surface.create(self.surface_w, self.surface_h)
  self.black_surface:fill_color({0, 0, 0})

  -- Configure moving birds.
  self.birds = {
    {
      y_begin = 80,
      y_end = 80,
      begin_side = "left",
      start_delay = 0,
      speed = 20,
    },
    {
      y_begin = 60,
      y_end = 60,
      begin_side = "right",
      start_delay = 4000,
      speed = 20,
    },
    {
      y_begin = 70,
      y_end = 70,
      begin_side = "right",
      start_delay = 6000,
      speed = 14,
    },
    {
      y_begin = 48,
      y_end = 48,
      begin_side = "left",
      start_delay = 5000,
      speed = 18,
    },
  }
  self.birds_started = false
  self:start_birds()

  self.water_img = sol.surface.create("menus/chasebliss/title_screen/water.png")
  self.water_img_w, self.water_img_h = self.water_img:get_size()
  local water_shader = sol.shader.create("distorsion")
  water_shader:set_uniform("magnitude", 0.0025)
  self.water_img:set_shader(water_shader)

  -- Black stripes
  self.black_stripe = sol.surface.create(self.surface_w, 24)
  self.black_stripe:fill_color({0, 0, 0})

  -- White surface (for the white flash)
  self.white_flash_img = sol.surface.create(self.surface_w, self.surface_h)
  self.white_flash_img:fill_color({255, 255, 255})

  -- Start timer.
  self.anim_length = 600
  self.anim_delta = 25
  self.elapsed_time = 0

  -- Start animation.
  self:step_1()
end

function chasebliss_title:inCubic(t, b, c, d)
  t = t / d
  return c * math.pow(t, 3) + b
end

function chasebliss_title:on_draw(dst_surface)

  local dst_w, dst_h = dst_surface:get_size()
  
  -- NB: the order is important here to get the layers in the correct order.
  
  -- flash et background
  if self.step >= 4 then
    -- water
    local water_img_x = (dst_w - self.water_img_w) / 2
    local water_img_y = dst_h / 2 + 54
    self.water_img:draw(dst_surface, water_img_x, water_img_y)
    
    -- background
    local background_img_x = (dst_w - self.background_img_w) / 2
    local background_img_y = (dst_h - self.background_img_h) / 2 - 13
    self.background_img:draw(dst_surface, background_img_x, background_img_y)

    -- birds
    for _, bird in pairs(self.birds) do
      if bird.sprite ~= nil then
        bird.sprite:draw(dst_surface)
      end
    end
    
    -- flash
    self.white_flash_img:draw(dst_surface, 0, 0)
  end
  
    local triforce_sprite_x = (dst_w - self.triforce_sprite_w) / 2
    local triforce_sprite_y = (dst_h - self.triforce_sprite_h) / 2
    self.triforce_sprite:draw(dst_surface, triforce_sprite_x, triforce_sprite_y)    

  -- guitar
  if self.step >= 3 then
    local logo_guitar_img_x = (dst_w - self.logo_guitar_img_w) / 2 - 50
    local logo_guitar_img_y = (dst_h - self.logo_guitar_img_h) / 2
    if self.step == 3 then
      logo_guitar_img_y = math.ceil(self:inCubic(self.elapsed_time, -self.logo_guitar_img_h, logo_guitar_img_y + self.logo_guitar_img_h, self.anim_length))
    end
    self.logo_guitar_img:draw(dst_surface, logo_guitar_img_x, logo_guitar_img_y)
  end
  
  -- bliss
  if self.step >= 2 then
    local logo_bliss_img_x = (dst_w - self.logo_bliss_img_w) / 2
    local logo_bliss_img_y = (dst_h - self.logo_bliss_img_h) / 2 + 9
    self.logo_bliss_img:draw(dst_surface, logo_bliss_img_x, logo_bliss_img_y)

    local logo_tlo_img_x = (dst_w - self.logo_tlo_img_w) / 2 + 10
    local logo_tlo_img_y = dst_h / 2 - 30
    self.logo_tlo_img:draw(dst_surface, logo_tlo_img_x, logo_tlo_img_y)
  end

  if self.step == 5 then
    self.shine_sprite:draw(dst_surface)
  end

  -- subtitle
  if self.step >= 4 then
    local logo_subtitle_img_x = (dst_w - self.logo_subtitle_img_w) / 2 + 14
    local logo_subtitle_img_y = (dst_h - self.logo_subtitle_img_h) / 2 + 46
    self.logo_subtitle_img:draw(dst_surface, logo_subtitle_img_x, logo_subtitle_img_y)
  end

  -- cinematic black stripes.
  self.black_stripe:draw(dst_surface, 0, 0)
  self.black_stripe:draw(dst_surface, 0, dst_h - 24)

  -- SNES logo
  -- if self.step >= 4 then
  --   local snes_img_x = (dst_w - self.snes_img_w) / 2 - 7
  --   local snes_img_y = 17
  --   self.snes_img:draw(dst_surface, snes_img_x, snes_img_y)
  -- end
  
  if self.step >= 6 then
    local press_start_sprite_x = (dst_w - self.press_start_sprite_w) / 2
    self.press_start_sprite:draw(dst_surface, press_start_sprite_x, 212)
  end

  -- copyright
  local copyright_img_x = (dst_w - self.copyright_img_w) / 2
  local copyright_img_y = dst_h - self.copyright_img_h - 9
  self.copyright_img:draw(dst_surface, copyright_img_x, copyright_img_y)

  -- Final fade-out
  -- if self.step >= 7 then
  --   self.black_surface:draw(dst_surface, 0, 0)    
  -- end
end

function chasebliss_title:reset_timer()
  self.elapsed_time = 0
  if self.timer ~= nil then
    self.timer:stop()
    self.timer = nil
  end
end

-- Triforce
function chasebliss_title:step_1()
  self.step = 1
  
  self:reset_timer()
  
  self.triforce_sprite:set_animation("moving", function()
    self.triforce_sprite:set_animation("static")
    self:step_2()
  end)
  
end

-- Logo fade-in
function chasebliss_title:step_2()
  self.step = 2

  self:reset_timer()

  self.triforce_sprite:set_animation("static")
  self.logo_tlo_img:fade_in(40)
  self.logo_bliss_img:fade_in(40, function()
    self:step_3()
  end)

end

-- Sword
function chasebliss_title:step_3()
  self.step = 3

  self:reset_timer()

  self.timer = sol.timer.start(self, self.anim_delta, function()
    -- Elapsed time since launch of animation.
    self.elapsed_time = self.elapsed_time + self.anim_delta

    if self.elapsed_time < self.anim_length then
      -- Keep on updating while time is remaining.
      return true
    else
      -- At the end of the animation, start the next step.
      if self.timer ~= nil then
        self.timer:stop()
        self.timer = nil
      end
      self:step_4()
    end
  end)

end

-- White flash
function chasebliss_title:step_4()
  self.step = 4

  self:reset_timer()
  
  self.logo_subtitle_img:fade_in(40)

  local fade_in_delay = 60
  self.snes_img:fade_in(fade_in_delay)
  self.white_flash_img:fade_out(fade_in_delay, function()
    self:step_5()
  end)
end

-- Little shine
function chasebliss_title:step_5()
  self.step = 5

  self:reset_timer()

  self.timer = sol.timer.start(500, function()
    self.shine_sprite:set_animation("default", function()
      self.timer:stop()
      self.timer = nil
      self.timer = sol.timer.start(500, function()
        self:step_6()
      end)
    end)
  end)
end

-- Press Start
function chasebliss_title:step_6()
  self.step = 6

  self:reset_timer()
  
  -- Stop the menu after a delay
  self.timer = sol.timer.start(4000, function()
    self:step_7()
  end)

end

-- Final fade-out
function chasebliss_title:step_7()
  self.step = 7

  self:reset_timer()

  -- self.black_surface:fade_in(60, function()
  --   self:skip_menu()
  -- end)
end


-- Start the birds movements.
function chasebliss_title:start_birds()
  if not self.birds_started then
    self.birds_started = true

    for _, bird in pairs(self.birds) do
      if bird.timer ~= nil then
        bird.timer:stop()
        bird.timer = nil
      end
      bird.timer = sol.timer.start(self, bird.start_delay, function()
        self:move_bird(bird)
      end)
    end
  end
end

-- Move a bird sprite on the screen.
function chasebliss_title:move_bird(bird)
  if bird.sprite == nil then
    bird.sprite = sol.sprite.create("menus/chasebliss/title_screen/bird")
    bird.sprite:set_animation("flying")

    local w, h = bird.sprite:get_size()
    if bird.begin_side == "left" then
      bird.sprite:set_xy(-w, bird.y_begin)
    else
      bird.sprite:set_xy(self.surface_w + w, bird.y_begin)
    end
  end

  if bird.movement == nil then
    bird.movement = sol.movement.create("target")
    bird.movement:set_speed(bird.speed)
    bird.movement:set_ignore_obstacles(true)
    bird.movement:set_smooth(true)
  end

  local x, y = bird.sprite:get_xy()
  local w, h = bird.sprite:get_size()
  if x >= self.surface_w then
    -- The bird is on the right side.
    bird.sprite:set_xy(self.surface_w + w, bird.y_begin)
    bird.sprite:set_direction(2)
    bird.movement:set_target(-w, bird.y_end)
  elseif x <= 0 then
    -- The bird is on the left side.
    bird.sprite:set_xy(-w, bird.y_begin)
    bird.sprite:set_direction(0)
    bird.movement:set_target(self.surface_w + w, bird.y_end)
  end

  -- Start the movement.
  if sol.menu.is_started(self) then
    bird.movement:start(bird.sprite, function()
      if bird.timer ~= nil then
        bird.timer:stop()
        bird.timer = nil
      end
      -- When the movement is done, wait a bit, then restart in the opposite direction.
      if sol.menu.is_started(self) then
        bird.timer = sol.timer.start(self, 2000, function()
          self:move_bird(bird)
        end)
      end
    end)
  end
end

-----------------------------------------------------------------------

-- Called when a keyboard key is pressed.
function chasebliss_title:on_key_pressed(key)

  if key == "escape" then
    -- Escape: quit Solarus.
    sol.main.exit()
    return true
  elseif self.finished then
    self:skip_menu()
    return true
  end
  
  return false
end

-- Mouse pressed: skip menu.
function chasebliss_title:on_mouse_pressed(button, x, y)
  if self.finished then
    if button == "left" or button == "right" then
      self:skip_menu()
    end
  end
end

function chasebliss_title:skip_menu()
  if not sol.menu.is_started(self) then
    return
  end

  -- Stop the timer.
  self:reset_timer()

  -- Quit the menu
  sol.menu.stop(self)
end

-- Return the menu to the caller
return chasebliss_title
