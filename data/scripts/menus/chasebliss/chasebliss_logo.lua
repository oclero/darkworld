-- Chase Bliss logo
local chasebliss_logo = {}

function chasebliss_logo:on_started()

  -- Load images.
  self.logo_img = sol.surface.create("menus/chasebliss/logo/chasebliss_logo.png")
  self.logo_w, self.logo_h = self.logo_img:get_size()
  
  self.catchphrase_img = sol.surface.create("menus/chasebliss/logo/chasebliss_catchphrase.png")
  self.catchphrase_w, self.catchphrase_h = self.catchphrase_img:get_size()
  self.draw_catchphrase = false

  self.has_played_gb_sound = false
  self.finished = true --false

  -- Start timer.
  self.anim_length = 2500
  self.anim_delta = 25
  self.elapsed_time = 0
  self.has_started = false

  -- Start animation.
  --self.timer = sol.timer.start(3000, function()
    self:step_1()
  --end)
end

function chasebliss_logo:outQuart(t, b, c, d)
  t = t / d - 1
  return -c * (math.pow(t, 4) - 1) + b
end

function chasebliss_logo:on_draw(dst_surface)

  if self.has_started then

    local dst_w, dst_h = dst_surface:get_size()

    local logo_x = (dst_w - self.logo_w) / 2
    local logo_y = ((dst_h - self.logo_h) / 2) - 24
    logo_y = math.ceil(self:outQuart(self.elapsed_time, -self.logo_h, logo_y + self.logo_h, self.anim_length))

    if self.elapsed_time > 1200 and not self.has_played_gb_sound then
      self.has_played_gb_sound = true
      --sol.audio.play_sound("chasebliss/gameboy_startup")
    end
    
    self.logo_img:draw(dst_surface, logo_x, logo_y)    

    if self.draw_catchphrase then
      local catchphrase_x = (dst_w - self.catchphrase_w) / 2
      local catchphrase_y = logo_y + self.logo_h + 24
      self.catchphrase_img:draw(dst_surface, catchphrase_x, catchphrase_y)    
    end
  end

end

function chasebliss_logo:step_1()
  self.elapsed_time = 0
  self.has_started = true

  if self.timer then
    self.timer:stop()
    self.timer = nil
  end

  self.timer = sol.timer.start(self, self.anim_delta, function()
    -- Elapsed time since launch of animation.
    self.elapsed_time = self.elapsed_time + self.anim_delta

    if self.elapsed_time < self.anim_length then
      -- Keep on updating while time is remaining.
      return true
    else
      -- At the end of the animation, start the next step.
      self.timer:stop()
      self.timer = nil
      self:step_2()
    end
  end)
end

function chasebliss_logo:step_2()
  if self.timer then
    self.timer:stop()
    self.timer = nil
  end

  self.timer = sol.timer.start(self, 500, function()
    --sol.audio.play_sound("zonzifleur/solarus_team_logo")
    self.draw_catchphrase = true
    self.catchphrase_img:fade_in()
    self.timer:stop()
    self.timer = nil
    self:step_3()
    self.finished = true
  end)
end

function chasebliss_logo:step_3()
  if self.timer then
    self.timer:stop()
    self.timer = nil
  end

  -- self.timer = sol.timer.start(self, 2000, function()  
  --   self.catchphrase_img:fade_out()
  --   self.logo_img:fade_out(20, function()
  --     self:skip_menu()
  --   end)
  -- end)
end

-- Called when a keyboard key is pressed.
function chasebliss_logo:on_key_pressed(key)

  if key == "escape" then
    -- Escape: quit Solarus.
    sol.main.exit()
    return true
  elseif self.finished then
    self:skip_menu()
    return true
  end
  
  return false
end

-- Mouse pressed: skip menu.
function chasebliss_logo:on_mouse_pressed(button, x, y)
  if self.finished then
    if button == "left" or button == "right" then
      self:skip_menu()
      sol.main.exit()
    end
  end
end

function chasebliss_logo:skip_menu()
  if not sol.menu.is_started(self) then
    return
  end

  -- Stop the timer.
  if self.timer ~= nil then
    self.timer:stop()
    self.timer = nil
  end

  -- Quit the menu
  sol.menu.stop(self)

end

-- Return the menu to the caller
return chasebliss_logo
