-- Defines the menus displayed when executing the quest,
-- before starting a game.

-- You can edit this file to add, remove or move some pre-game menus.
-- Each element must be the name of a menu script.
-- The last menu is supposed to start a game.

local initial_menus = {
  --"scripts/menus/language",
  "scripts/menus/solarus_logo",
  "scripts/menus/team_logo",
  "scripts/menus/chasebliss/chasebliss_logo",
  "scripts/menus/chasebliss/chasebliss_title",
  --"scripts/menus/chasebliss/chasebliss_pedal",
}

return initial_menus
