-- Script that creates a pause menu for a game.

-- Usage:
-- require("scripts/menus/pause")

require("scripts/multi_events")

-- Creates a pause menu for the specified game.
local function initialize_pause_features(game)

  if game.pause_menu ~= nil then
    -- Already done.
    return
  end

  local pause_menu = {}
  game.pause_menu = pause_menu

  function pause_menu:on_started()

    -- Play the sound of pausing the game.
    sol.audio.play_sound("pause_open")

  end

  function pause_menu:open()
    sol.menu.start(game, pause_menu, false)
  end

  function pause_menu:close()
    sol.menu.stop(pause_menu)
  end

  function pause_menu:on_finished()

    -- Play the sound of unpausing the game.
    sol.audio.play_sound("pause_closed")

    game.pause_submenus = {}
    -- Restore opacity
    game:get_hud():set_item_icon_opacity(1, 255)
    game:get_hud():set_item_icon_opacity(2, 255)
    -- Restore the built-in effect of action and attack commands.
    if game.set_custom_command_effect ~= nil then
      game:set_custom_command_effect("action", nil)
      game:set_custom_command_effect("attack", nil)
    end
  end

  game:register_event("on_paused", function(game)
    pause_menu:open()
  end)
  game:register_event("on_unpaused", function(game)
    pause_menu:close()
  end)

end

-- Set up the pause menu on any game that starts.
local game_meta = sol.main.get_metatable("game")
game_meta:register_event("on_started", initialize_pause_features)

return true
