-- Lua script of map chasebliss/dungeon_in.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local pedal_menu = require("scripts/menus/chasebliss/chasebliss_pedal")

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()

  -- You can initialize the movement and sprites of various
  -- map entities here.

end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()
  
end

function chest_pedal:on_opened(item, variant, variable)
  local hero = game:get_hero()
  local hero_sprite = hero:get_sprite()

  -- Block
  game:set_hud_enabled(false)
  local backup_hero_ignore_suspended = hero_sprite:get_ignore_suspend()
  hero_sprite:set_ignore_suspend(true)
  
  -- Move the pedal
  pedal:set_enabled(true)
  local pedal_sprite = pedal:get_sprite()
  pedal_sprite:set_animation("shining")
  pedal_sprite:set_ignore_suspend(true)
  local pedal_movement = sol.movement.create("target")
  pedal_movement:set_ignore_suspend(true)
  pedal_movement:set_ignore_obstacles(true)
  pedal_movement:set_smooth(true)
  local pedal_x, pedal_y = pedal:get_position()
  pedal_movement:set_target(pedal_x, pedal_y - 8)
  pedal_movement:set_speed(16) -- pixels per second

  -- Launch everything
  --game:set_suspended(true)
  hero_sprite:set_animation("brandish")
  pedal_movement:start(pedal, function()
    game:start_dialog("_treasure.chasebliss_pedal.1", function()
      --game:set_hud_enabled(true)
      --game:set_suspended(false)
      --hero_sprite:set_ignore_suspend(backup_hero_ignore_suspended)
      pedal:set_enabled(false)
      hero_sprite:set_animation("stopped")
      sol.menu.start(map, pedal_menu)
    end)
  end)
  
end
